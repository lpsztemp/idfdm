### Print out Intel DisplayFamily-DisplayModel

Uses the CPUID instruction to obtain and print-out `DisplayFamily` and `DisplayModel` values as specified in the vol. 2A, section 3.2, of Intel® 64 and IA-32 Architectures Software Developer’s Manual 2021.
See figure 3.6 of the manual.

The format of the output corresponds to one used in the table 2-1 of the vol. 4 of the manual.
For instance, the program displays `06_55H` when executed by the Intel Cascade Lake CPU.

The program fails if the CPU is not Intel or does not implement CPUID.01 leaf.
