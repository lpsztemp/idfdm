.code

get_family_model proc
push rbx
push rbp
mov rbp, rsp
pushfq
mov rax, [rsp]
btc rax, 21
mov [rsp], rax
popfq
pushfq
xor rax, [rsp]
jnz fail
xor eax, eax
cpuid
test eax, eax
jz finish
cmp ebx, 756e6547h
jnz fail
cmp ecx, 6c65746eh
jnz fail
cmp edx, 49656e69h
jnz fail
mov eax, 1
cpuid
and eax, 0FFF0FF0h
mov r8d, eax
mov r9d, eax
shr r8d, 4
and r8d, 0Fh
shr r9d, 12
and eax, 0F00h
cmp eax, 0F00h
jz model_id_add
cmp eax, 600h
jnz family_id_check
and r9d, 0FFh
model_id_add:
add eax, r9d
add eax, r8d
jmp finish
family_id_check:
or eax, r8d
jmp finish
fail:
xor ax, ax
finish:
mov rsp, rbp
pop rbp
pop rbx
ret
get_family_model endp
end
