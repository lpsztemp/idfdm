#include <stdio.h>

short get_family_model();

int main(int argc, char** argv) {
	short df_dm = get_family_model();
	if (!df_dm) {
		fprintf(stderr, "ERROR: Unsupported processor.\n");
		return __LINE__;
	}
	printf("%02X_%02XH\n", df_dm >> 8, df_dm & 0xFF);
	return 0;
}