.586
.model flat
.code

_get_family_model proc
push ebx
push ebp
mov ebp, esp
pushfd
mov eax, [esp]
btc eax, 21
mov [esp], eax
popfd
pushfd
xor eax, [esp]
jnz fail
xor eax, eax
cpuid
test eax, eax
jz finish
cmp ebx, 756e6547h
jnz fail
cmp ecx, 6c65746eh
jnz fail
cmp edx, 49656e69h
jnz fail
mov eax, 1
cpuid
and eax, 0FFF0FF0h
mov ebx, eax
mov ecx, eax
shr ebx, 4
and ebx, 0Fh
shr ecx, 12
and eax, 0F00h
cmp eax, 0F00h
jz model_id_add
cmp eax, 600h
jnz family_id_check
and ecx, 0FFh
model_id_add:
add eax, ecx
add eax, ebx
jmp finish
family_id_check:
or eax, ebx
jmp finish
fail:
xor ax, ax
finish:
mov esp, ebp
pop ebp
pop ebx
ret
_get_family_model endp
end
