.phony: all clean

all:idfdm

clean:
	rm -f *.obj idfdm

idfdm:asx64.s entrypoint.c
	gcc -o $@ $^
