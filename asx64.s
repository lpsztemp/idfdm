.globl get_family_model
.text
get_family_model:
push %rbx
push %rbp
mov %rsp, %rbp
pushfq
mov (%rsp), %rax
btc $21, %rax
mov %rax, (%rsp)
popfq
pushfq
xor (%rsp), %rax
jnz fail
xor %rax, %rax
cpuid
test %eax, %eax
jz finish
cmp $0x756e6547, %ebx
jnz fail
cmp $0x6c65746e, %ecx
jnz fail
cmp $0x49656e69, %edx
jnz fail
mov $1, %eax
cpuid
and $0xFFF0FF0, %eax
mov %eax, %r8d
mov %eax, %r9d
shr $4, %r8d
and $0xF, %r8d
shr $12, %r9d
and $0xF00, %eax
cmp $0xF00, %eax
jz model_id_add
cmp $0x600, %eax
jnz family_id_check
and $0xFF, %r9d
model_id_add:
add %r9d, %eax
add %r8d, %eax
jmp finish
family_id_check:
or %r8d, %eax
jmp finish
fail:
xor %ax, %ax
finish:
mov %rbp, %rsp
pop %rbp
pop %rbx
ret

